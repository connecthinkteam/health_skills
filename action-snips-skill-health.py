#!/usr/bin/env python3
from hermes_python.hermes import Hermes 
import requests

MQTT_IP_ADDR = "localhost" 
MQTT_PORT = 1883 
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT)) 


def extraer_contenido():
    max = 3
    noticias = ""
    cabeceras = ""
    url = "http://blogs.terrassa.cat/activa60/?feed=rss2"
    response = requests.get(url)
    webContent = response.text.encode('utf-8')
    webContent = webContent.decode()
    webContent = webContent.replace('<item>','@')
    webContent = webContent.split('@')
    for x in webContent:
        if webContent.index(x) == 0:
            continue
        if webContent.index(x) > max:
            break
        tit = x.replace('<title>','@')
        tit = tit.replace('</title>','@')
        tit = tit.replace('<description>','@')
        tit = tit.replace('</description>','@')
        tit = tit.split('@')
        especial = "&#" in tit[1]
        if not especial:
            cabeceras += tit[1] + ".\r\n"
        #noticias += tit[1] + ".\r\n" + tit[3] + ".\r\n"
        #result = [cabeceras, noticias]
    return cabeceras

def intent_received(hermes, intent_message):
    mensaje = extraer_contenido()
    
    if intent_message.intent.intent_name == 'xavixicota:findDoctor':
        sentence = 'Tu Medico es el doctor Xicota'
    elif intent_message.intent.intent_name == 'xavixicota:findMedication':
        sentence = 'Hoy debes tomarte treinta y dos pastillas'

    elif intent_message.intent.intent_name == 'xavixicota:findActivity':
        sentence = 'Hoy tienes clase de petanca y ademas' + mensaje                   
    else:
        return
    
    hermes.publish_end_session(intent_message.session_id, sentence)
    
    
with Hermes(MQTT_ADDR) as h:
    h.subscribe_intents(intent_received).start()